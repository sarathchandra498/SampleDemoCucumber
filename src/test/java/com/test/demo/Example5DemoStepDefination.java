package com.test.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Example5DemoStepDefination {
	public static WebDriver driver;
	
	
	@Before
    public void beforeScenario(){
        System.out.println("This will run before the every Scenario");
    }
 
	@After
    public void afterScenario(){
        System.out.println("This will run after the every Scenario");
    }
 
	@Before("@FirstTest")
    public void beforeFirst(){
        System.out.println("This will run only before the First Scenario");
    }	
 
	@Before("@secondTest")
    public void beforeSecond(){
        System.out.println("This will run only before the Second Scenario");
    }
	
	@Given("^Open (.*)$")
	public void open_Chrome(String arg1) {
		System.setProperty("webdriver.chrome.driver", "D:/selinium/chromedriver.exe");
        driver = new ChromeDriver();
	}

	@Given("^Navigate To URL$")
	public void navigate_To_URL() {
		driver.get("https://gitlab.com/users/sign_in");
	}
	
	@Given("^Navigate To sss$")
	public void navigate_To_URL(String arg1) {
		System.out.println(arg1);
		driver.get("https://gitlab.com/users/sign_in");
	}

	@When("^enter userName and Password$")
	public void enter_userName_and_Password() {
	  	driver.findElement(By.id("user_login")).sendKeys("asda");
		driver.findElement(By.id("user_password")).sendKeys("qeweq");
	}

	@Then("^Click login button$")
	public void click_login_button() {
		driver.findElement(By.name("commit")).click();
	}

	@Then("^Error Message should be displayed$")
	public void error_Message_should_be_displayed() throws InterruptedException {
		Thread.sleep(5000);
		WebElement a=	driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/div/div/span"));
		System.out.println(a.getText());
	}

@When("^enter  sarathchandra(\\d+) and Happy@(\\d+)$")
	public void enter_and_with_valid(String arg1, String arg2) {
		driver.findElement(By.id("user_login")).sendKeys(arg1);
		driver.findElement(By.id("user_password")).sendKeys(arg2);
	}

	@Then("^User should get login successful$")
	public void user_should_get_login_successful() throws InterruptedException {
		//Thread.sleep(5000);
		  //System.out.println(driver.findElement(By.className("current-user")).getText());
	}

}
