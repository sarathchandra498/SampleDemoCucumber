@Example1
Feature: validate  Git Login 
  I want to login git lab using my credentails
  Scenario: Validate Git lab Login with invalid credentails in chrome
    Given Open Browser in chrome
    And Navigate To URL
    When enter userName and Password
    Then Click login button
    Then Error Message should be displayed
  Scenario: Validate Git lab Login with invalid credentails in firefox
    Given Open Browser in firefox
    And Navigate To URL
    When enter userName and Password
    Then Click login button
    Then Error Message should be displayed