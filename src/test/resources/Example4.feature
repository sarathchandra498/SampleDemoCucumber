@Example4
Feature: validate  Git Login 
  I want to login git lab using my credentails
  Scenario: Validate Git lab Login with invalid credentails in chrome
    Given Open Browser in chrome
    And Navigate To URL
    When enter userName and Password:
    | fields              | values              |
		| m8853               | sASSSS              |
		| ASDASDAS            | asda                |
    Then Click login button
    Then Error Message should be displayed