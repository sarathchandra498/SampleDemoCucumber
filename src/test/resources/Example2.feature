@Example2
Feature: validate  Git Login 
  I want to login git lab using my credentails
  Scenario Outline: Validate Git lab Login with invalid credentails
    Given Open <browser>
    And Navigate To URL
    When enter userName and Password
    Then Click login button
    Then Error Message should be displayed
 Examples:
    | browser |
    | Chrome  |
    | FireFox |
 
 
 Scenario Outline: Validate Git lab Login with valid credentails
    Given Open <browser>
    And Navigate To URL
    When enter  <username> and <password> with valid
    Then Click login button
    Then User should get login successful
 Examples:
    | browser | username           | password     |
    | Chrome  | sarathchandra498   | Happy@39     |
  