@Example3
Feature: validate  Git Login 
  I want to login git lab using my credentails
  
  Background: 
    Given Open chrome browser
    And Navigate To URL
  Scenario Outline: Validate Git lab Login with invalid credentails
    When enter  <username2> and <password2>
    Then Click login button
    Then Error Message should be displayed
 Examples:
    | username2 | password2 |
    | dasda     | asda      |
    
 
 
 Scenario Outline: Validate Git lab Login with valid credentails
    When enter  <username1> and <password1>
    Then Click login button
    Then User should get login successful
 Examples:
    | username1           | password1     |
    | sarathchandra498    | Happy@39      |
  